## Writeup Template

### You can use this file as a template for your writeup if you want to submit it as a markdown file, but feel free to use some other method and submit a pdf if you prefer.

---

**Advanced Lane Finding Project**

The goals / steps of this project are the following:

* Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
* Apply a distortion correction to raw images.
* Use color transforms, gradients, etc., to create a thresholded binary image.
* Apply a perspective transform to rectify binary image ("birds-eye view").
* Detect lane pixels and fit to find the lane boundary.
* Determine the curvature of the lane and vehicle position with respect to center.
* Warp the detected lane boundaries back onto the original image.
* Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

[//]: # (Image References)

[image1]: ./output_images/un_distortion_road.png "Undistorted"
[image2]: ./output_images/img_unwarp.jpg "Road Transformed"
[image3]: ./output_images/bin.jpg "Binary Example"
[image4]: ./output_images/img_unwarp.jpg "Warp Example"
[image5]: ./output_images/final.jpg "Fit Visual"
[image6]: ./output_images/final.jpg "Output"
[video1]: ./project_video_output.mp4 "Video"

## [Rubric](https://review.udacity.com/#!/rubrics/571/view) Points

### Here I will consider the rubric points individually and describe how I addressed each point in my implementation.  

---

### Writeup / README

#### 1. Provide a Writeup / README that includes all the rubric points and how you addressed each one.  You can submit your writeup as markdown or pdf.  [Here](https://github.com/udacity/CarND-Advanced-Lane-Lines/blob/master/writeup_template.md) is a template writeup for this project you can use as a guide and a starting point.  

You're reading it!

### Camera Calibration

#### 1. Briefly state how you computed the camera matrix and distortion coefficients. Provide an example of a distortion corrected calibration image.

The code for this step is contained in the first code cell of the IPython notebook located in "./examples/example.ipynb" (or in lines # through # of the file called `some_file.py`).  

I start by preparing "object points", which will be the (x, y, z) coordinates of the chessboard corners in the world. Here I am assuming the chessboard is fixed on the (x, y) plane at z=0, such that the object points are the same for each calibration image.  Thus, `objp` is just a replicated array of coordinates, and `objpoints` will be appended with a copy of it every time I successfully detect all chessboard corners in a test image.  `imgpoints` will be appended with the (x, y) pixel position of each of the corners in the image plane with each successful chessboard detection.  

I then used the output `objpoints` and `imgpoints` to compute the camera calibration and distortion coefficients using the `cv2.calibrateCamera()` function.  I applied this distortion correction to the test image using the `cv2.undistort()` function and obtained this result: 

![alt text][image1]

### Pipeline (single images)

#### 1. Provide an example of a distortion-corrected image.

To demonstrate this step, I will describe how I apply the distortion correction to one of the test images like this one:
![alt text][image2]

Camera calibration by using test bench: chessboards, you will see the results images has corner points and lines connected
Compute the cameara calibration matrix and distortion coefficients givena set of chessboard images
Camera calibration undistort from example
Apply a distortion correction to raw images, Select one image from test set

#### 2. Describe how (and identify where in your code) you used color transforms, gradients or other methods to create a thresholded binary image.  Provide an example of a binary image result.

I used a combination of color and gradient thresholds to generate a binary image (thresholding steps at lines # through # in `another_file.py`).  Here's an example of my output for this step.  (note: this is not actually from one of the test images)
'''python
     Pass in your image into this function
    Write code to do the following steps
    1) Undistort using mtx and dist
    #img2 = cv2.undistort(img, mtx, dist, None, mtx)
    2) Convert to grayscale
    #gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
    3) Find the chessboard corners
    #ret, corners = cv2.findChessboardCorners(gray, (nx,ny), None)
    4) If corners found: 
    #if ret == True:
        a) draw corners
    #img2 = cv2.drawChessboardCorners(img2, (nx,ny), corners, ret)
        b) define 4 source points src = np.float32([[,],[,],[,],[,]])
             #Note: you could pick any four of the detected corners 
             as long as those four corners define a rectangle
             #One especially smart way to do this would be to use four well-chosen
             corners that were automatically detected during the undistortion steps
             #We recommend using the automatic detection of corners in your code
     src = np.float32([corners[0],
                         corners[nx - 1], 
                         corners[ny*nx - nx], 
                         corners[ny*nx -1]]) 
        c) define 4 destination points dst = np.float32([[,],[,],[,],[,]])
'''

![alt text][image3]

#### 3. Describe how (and identify where in your code) you performed a perspective transform and provide an example of a transformed image.

The code for my perspective transform includes a function called `warper()`, which appears in lines 1 through 8 in the file `example.py` (output_images/examples/example.py) (or, for example, in the 3rd code cell of the IPython notebook).  The `warper()` function takes as inputs an image (`img`), as well as source (`src`) and destination (`dst`) points.  I chose the hardcode the source and destination points in the following manner:

```python
    src = np.float32([(575,464),
                      (707,464), 
                      (258,682), 
                      (1049,682)])
    dst = np.float32([(450,0),
                      (w-450,0),
                      (450,h),
                      (w-450,h)])
```


I verified that my perspective transform was working as expected by drawing the `src` and `dst` points onto a test image and its warped counterpart to verify that the lines appear parallel in the warped image.

![alt text][image4]

#### 4. Describe how (and identify where in your code) you identified lane-line pixels and fit their positions with a polynomial?

Then I did some other stuff and fit my lane lines with a 2nd order polynomial kinda like this:

The original idea was from : https://github.com/jeremy-shannon
But He used on parameter in his function, right now, I am using two color space which are L: lightness
B: blue-yellow color components, actualy you can use RGB, or HSV to finished based on plotting and testing

The return image is not binary image, but only has the lanes and little noise, I tune different thresholds
to test them, still do not know what thereshols are best for this project

Define a function that thresholds the B-channel of LAB
Use exclusive lower bound (>) and inclusive upper (<=), OR the results of the thresholds (B channel should capture
yellows)
![alt text][image5]

#### 5. Describe how (and identify where in your code) you calculated the radius of curvature of the lane and the position of the vehicle with respect to center.

I did this in lines # through # in my code in `my_other_file.py`
Method to determine radius of curvature and distance from lane center 
based on binary image, polynomial fit, and L and R lane pixel indices

#### 6. Provide an example image of your result plotted back down onto the road such that the lane area is identified clearly.

I implemented this step in lines # through # in my code in `yet_another_file.py` in the function `map_lane()`.  Here is an example of my result on a test image:

![alt text][image6]

---

### Pipeline (video)

#### 1. Provide a link to your final video output.  Your pipeline should perform reasonably well on the entire project video (wobbly lines are ok but no catastrophic failures that would cause the car to drive off the road!).

Here's a [link to my video result](./project_video_output.mp4)

---

### Discussion

#### 1. Briefly discuss any problems / issues you faced in your implementation of this project.  Where will your pipeline likely fail?  What could you do to make it more robust?

Here I'll talk about the approach I took, what techniques I used, what worked and why, where the pipeline might fail and how I might improve it if I were going to pursue this project further.  

Sorry, I updated the discussion about the code issues and problems last time. I did not why I click submit you did not see it. Maybe I did it in wrong way.

Anyway, I will write it one more time. 

In the pipline about image frames processing, I was using the LAB color space to filter other noise except lane lines, but there are still many noise around it. So it does not work inthe night version very well. In the future, I need to analyze each frame in RGB, HSV, HSL colorsapce, etc.

In tht lane lines fitting and find labe line based on previous lane framses, the code can not work in the road segment, because the code pipline onl search the limit range from the previous image. It is faster but losing accuracy. Actually, it is hard to detect straight segment lane lines in my pipline. The possible solution from my side is to use Mathematical morphology for each image. such as Erosion, Dilation, Opening, Closing.

And honestly, I did not try challenge video for this project. For some reason, work and life are so busy every single day. No scial life at all. Thanks my instructor.



















